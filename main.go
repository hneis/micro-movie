package main

import (
	"encoding/json"
	"log"
	"net/http"
	"strconv"
	"strings"
	"time"

	"github.com/gorilla/mux"
)

var Addr = ":8084"

func main() {
	r := mux.NewRouter()
	r.HandleFunc("/movies", movieListHandler)
	log.Printf("Starting on port %s", Addr)
	log.Fatal(http.ListenAndServe(Addr, r))
}

type Movie struct {
	ID          int       `json:"id"`
	Name        string    `json:"name"`
	Poster      string    `json:"poster"`
	MovieUrl    string    `json:"movie_url"`
	IsPaid      bool      `json:"is_paid"`
	ReleaseYear time.Time `json:"release_year"`
	Genre       string    `json:"genre"`
}

var db = []Movie{
	Movie{0, "Бойцовский клуб", "/static/poster/fightclub.jpg", "https://youtu.be/qtRkdVHc-cE", true, timeMustParse("1999"), "triller"},
	Movie{1, "Крестный отец", "/static/poster/father.jpg", "https://youtu.be/ar1SHxgeZUc", true, timeMustParse("1998"), "drama"},
	Movie{2, "Криминальное чтиво", "/static/poster/pulpfiction.jpg", "https://youtu.be/s7EdQ4FqbhY", true, timeMustParse("1996"), "comedy"},
}

func timeMustParse(year string) time.Time {
	t, err := time.Parse("2006", year)
	if err != nil {
		panic(err)
	}
	return t
}

func movieListHandler(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json; charset=utf-8")

	var res []Movie
	for _, movie := range db {
		genres := r.URL.Query().Get("genres")
		if genres != "" {
			for _, genre := range strings.Split(genres, ",") {
				if movie.Genre == genre {
					res = append(res, movie)
				}
			}

		}
	}

	err := json.NewEncoder(w).Encode(res)

	if err != nil {
		log.Printf("Render response error: %v", err)
		w.WriteHeader(http.StatusInternalServerError)
	}
	return
}

